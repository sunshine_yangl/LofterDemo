
package com.lofterdemo;

import java.util.Collections;
import java.util.LinkedList;
import java.util.List;
import java.util.Random;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.os.Handler;
import android.view.View;
import android.widget.ImageView;

import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.assist.ImageScaleType;
import com.nostra13.universalimageloader.core.assist.SimpleImageLoadingListener;
import com.nostra13.universalimageloader.core.display.FadeInBitmapDisplayer;

/**
 * @author yushilong
 * @date 2014-10-22 上午11:07:18
 * @version 1.0
 */
public class SplashActivity extends Activity
{
    Handler mHandler;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        // TODO Auto-generated method stub
        super.onCreate(savedInstanceState);
        setContentView(R.layout.splash);
        final String imgUrl1 = "http://d.hiphotos.baidu.com/zhidao/pic/item/4034970a304e251ffe689b3fa586c9177e3e535d.jpg";
        final String imgUrl2 = "http://attach.bbs.miui.com/forum/201401/03/151241i8h3o73470bz7lcb.png";
        final String imgUrl3 = "http://attach2.scimg.cn/forum/201407/06/110014xmpwmtkfmd76e4fm.jpg";
        final String imgUrl4 = "http://e.hiphotos.baidu.com/image/h%3D1050%3Bcrop%3D0%2C0%2C1680%2C1050/sign=d2261b749c510fb367197397ec03f3f6/8d5494eef01f3a29191e80039b25bc315d607c2c.jpg";
        String[] imgs = { imgUrl1, imgUrl2, imgUrl3, imgUrl4 };
        mHandler = new Handler();
        ImageLoader.getInstance().displayImage(imgs[new Random().nextInt(4)], (ImageView) findViewById(R.id.image), displayImageOptions, new AnimateFirstDisplayListener());
        mHandler.postDelayed(new Runnable()
        {
            public void run()
            {
                startActivity(new Intent(SplashActivity.this, MainActivity.class));
                finish();
            }
        }, 4000l);
    }

    public static class AnimateFirstDisplayListener extends SimpleImageLoadingListener
    {
        static final List<String> displayedImages = Collections.synchronizedList(new LinkedList<String>());

        @Override
        public void onLoadingComplete(String imageUri , View view , Bitmap loadedImage)
        {
            if (loadedImage != null)
            {
                ImageView imageView = (ImageView) view;
                boolean firstDisplay = !displayedImages.contains(imageUri);
                FadeInBitmapDisplayer.animate(imageView, 3500);
                if (firstDisplay)
                {
                    displayedImages.add(imageUri);
                }
            }
        }
    }
    DisplayImageOptions displayImageOptions = new DisplayImageOptions.Builder().imageScaleType(ImageScaleType.EXACTLY).cacheInMemory(true).cacheOnDisc(true).bitmapConfig(Bitmap.Config.RGB_565).resetViewBeforeLoading(true).build();
}
