
package com.lofterdemo;

import android.app.Activity;
import android.os.Bundle;
import android.view.GestureDetector;
import android.view.GestureDetector.SimpleOnGestureListener;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnTouchListener;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.LinearLayout;

public class MainActivity extends Activity
{
    ImageView iv_tag, image;
    Animation mAnimationSet;
    LinearLayout mLinearLayout;
    boolean isStart;
    GestureDetector mGestureDetector;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        mLinearLayout = (LinearLayout) findViewById(R.id.parent);
        iv_tag = (ImageView) findViewById(R.id.iv_tag);
        image = (ImageView) findViewById(R.id.image);
        mAnimationSet = AnimationUtils.loadAnimation(this, R.anim.mainanimset);
        iv_tag.setOnClickListener(new OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                // TODO Auto-generated method stub
                iv_tag.startAnimation(mAnimationSet);
            }
        });
        mGestureDetector = new GestureDetector(this, new MyGestureDetector());
        image.setOnTouchListener(new OnTouchListener()
        {
            @Override
            public boolean onTouch(View v , MotionEvent event)
            {
                // TODO Auto-generated method stub
                mGestureDetector.onTouchEvent(event);
                return true;
            }
        });
    }

    class MyGestureDetector extends SimpleOnGestureListener
    {
        @Override
        public boolean onDoubleTap(MotionEvent e)
        {
            // TODO Auto-generated method stub
            System.out.println("onDoubleTap");
            int x = (int) e.getRawX();
            int y = (int) e.getRawY();
            int w = iv_tag.getWidth();
            int h = iv_tag.getHeight();
            iv_tag.layout(x - w / 2, y - h / 2, x + w / 2, y + h / 2);
            iv_tag.startAnimation(mAnimationSet);
            return false;
        }
    }
}
