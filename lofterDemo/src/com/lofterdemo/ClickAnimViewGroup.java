
package com.lofterdemo;

import android.content.Context;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.ViewGroup;
import android.widget.LinearLayout;

/**
 * @author yushilong
 * @date 2014-10-22 下午1:18:16
 * @version 1.0
 */
public class ClickAnimViewGroup extends LinearLayout
{
    public ClickAnimViewGroup(Context context, AttributeSet attrs, int defStyle)
    {
        super(context, attrs, defStyle);
        // TODO Auto-generated constructor stub
        init(context, attrs);
    }

    public ClickAnimViewGroup(Context context, AttributeSet attrs)
    {
        super(context, attrs);
        // TODO Auto-generated constructor stub
        init(context, attrs);
    }

    public ClickAnimViewGroup(Context context)
    {
        super(context);
        // TODO Auto-generated constructor stub
        init(context, null);
    }

    private void init(Context context , AttributeSet attrs)
    {
        // TODO Auto-generated method stub
        LayoutInflater.from(context).inflate(R.layout.clickanim, this);
    }
}
